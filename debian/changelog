irsim (9.7.104-1) unstable; urgency=medium

  * New upstream release
  * debian/control:
    - New standards version 4.4.1 - no changes
    - DH level 12
    - Use debhelper-compat
  * debian/patches/0007-Fix-cross-build.patch (Closes: #941324)
    - Thanks Helmut Grohne

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sat, 19 Oct 2019 20:07:42 +0200

irsim (9.7.101-1) unstable; urgency=medium

  * New upstream release

 -- Ruben Undheim <ruben.undheim@gmail.com>  Thu, 20 Dec 2018 10:40:53 +0100

irsim (9.7.100-1) unstable; urgency=medium

  * New upstream release
    - Refresh patches (using 'gbp pq')
  * Adopt orphaned package into the Debian Electronics team
    with myself as the only uploader. (Closes: #812628)
  * debian/autoreconf:
    - specify path of autoreconf files such that dh-autoreconf works correctly
  * debian/compat: level 11
  * debian/control:
    - Priority changed to optional
    - Maintainer changed to "Debian Electronics Team".
    - Added "Ruben Undheim" as uploader
    - debhelper >= 11
    - New standards version 4.2.1 - no changes
    - Added VCS URLs pointing to Salsa.
    - Upper-case first letter in short description
  * debian/copyright:
    - Use secure URL in Format field
    - Added new debian copyright holder
  * debian/gbp.conf: to enforce pristine-tar with gbp
  * debian/lintian-overrides: ignore one spelling error
  * debian/patches/05-reproducible-build.patch:
    - Patch for reproducible build updated (Closes: #829628)
      Thanks "Chris Lamb"!
  * debian/patches/0006-Fix-spelling-errors.patch:
    - Fix a number of detected spelling errors
  * debian/rules:
    - Add flags for hardening
    - Replace '--with autotools_dev' with '--with autoreconf'

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sun, 02 Sep 2018 23:30:30 +0200

irsim (9.7.93-2) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer. (see #812628)
  * Migrated DH level to 10.
  * debian/control:
      - Bumped Standards-Version to 4.0.0.
  * debian/watch: updated version to 4.
  * Run wrap-and-sort -a.

 -- Felipe Barbosa Alves <barbosa.felipe10@gmail.com>  Mon, 04 Sep 2017 14:26:33 -0300

irsim (9.7.93-1) unstable; urgency=medium

  * New upstream release
  * debian/control: Standards-Version: 3.9.6
  * Added 05-reproducible-build.patch to make build reproducible. Thanks to
    Chris Lamb (Closes: #778267)

 -- Roland Stigge <stigge@antcom.de>  Sat, 16 Jan 2016 16:39:46 +0100

irsim (9.7.87-1) unstable; urgency=medium

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Sat, 07 Jun 2014 16:13:37 +0200

irsim (9.7.84-1) unstable; urgency=medium

  * New upstream release
  * debian/control: Standards-Version: 3.9.5

 -- Roland Stigge <stigge@antcom.de>  Thu, 29 May 2014 17:42:57 +0200

irsim (9.7.82-2) unstable; urgency=low

  * Use autotools-dev to update config.{sub,guess} (Closes: #727389)

 -- Roland Stigge <stigge@antcom.de>  Thu, 24 Oct 2013 11:30:39 +0200

irsim (9.7.82-1) unstable; urgency=low

  * New upstream release
  * Use readlink for /usr/bin/wish in irsim to use versioned wish
    (Closes: #725700)

 -- Roland Stigge <stigge@antcom.de>  Thu, 17 Oct 2013 10:35:16 +0200

irsim (9.7.79-2) unstable; urgency=low

  * debian/control: Standards-Version: 3.9.4

 -- Roland Stigge <stigge@antcom.de>  Wed, 15 May 2013 21:50:57 +0200

irsim (9.7.79-1) experimental; urgency=low

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Fri, 25 Jan 2013 11:12:10 +0100

irsim (9.7.78-1) experimental; urgency=low

  * New upstream release

 -- Roland Stigge <stigge@antcom.de>  Tue, 25 Sep 2012 13:15:15 +0200

irsim (9.7.75-1) unstable; urgency=low

  * New upstream release
  * debian/compat: 9
  * debian/control: Standards-Version: 3.9.3

 -- Roland Stigge <stigge@antcom.de>  Sat, 10 Mar 2012 20:43:11 +0100

irsim (9.7.74-1) unstable; urgency=low

  * Initial release (Closes: #478112)

 -- Roland Stigge <stigge@antcom.de>  Wed, 27 Jul 2011 13:18:46 +0200
